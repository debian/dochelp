/****************************************************************************/
/*  Copyright © 2012-2015 Mehdi Dogguy <mehdi@debian.org>                   */
/*                                                                          */
/*  This file is part of Dochelp.                                           */
/*                                                                          */
/*  Dochelp is free software: you can redistribute it and/or modify it      */
/*  under the terms of the GNU General Public License as published by the   */
/*  Free Software Foundation, either version 3 of the License, or (at your  */
/*  option) any later version.                                              */
/*                                                                          */
/*  Dochelp is distributed in the hope that it will be useful, but WITHOUT  */
/*  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   */
/*  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   */
/*  for more details.                                                       */
/*                                                                          */
/*  You should have received a copy of the GNU General Public License       */
/*  along with Dochelp.  If not, see <http://www.gnu.org/licenses/>.        */
/****************************************************************************/

/*
 *  Dependencies: jquery.
 */

function update_count() {
    $("#doc_count").html($(".document").filter(":visible").length+" document(s) found");
}

function select_text() {
    return $("#search_select option:selected").text();
}

function select_change() {
    var selected = select_text();
    if (selected == "All") {
        $(".document").show();
    } else {
        $(".section").each(function(){
            if ($(this).text() == selected) {
                $(this).parent().parent().show();
                $(this).parent().parent().addClass("mark");
            } else {
                $(this).parent().parent().hide();
                $(this).parent().parent().removeClass("mark");
            }
        });
    };
    update_count();
}

function search_document(input) {
    var regexp = new RegExp(input, 'i');
    $(".title").each(function() {
        if ($(this).parent().parent().hasClass("mark") || select_text() == "All") {
            if (regexp.test($(this).text())) {
                $(this).parent().parent().show();
            } else {
                $(this).parent().parent().hide();
            }
        }
    });
    update_count();
}

function init () {
    $(".document").each(function() { $(this).addClass("mark"); });
    update_count();
    $("#search_form").show();
    $("#search_input").keyup(function(){
        if ($(this).val() != "") {
            search_document($(this).val());
        } else {
            if ($(this).val() == "") $(".document").show();
            $(".document .mark").filter(":hidden").show();
        }
        update_count();
    });
    $("#search_select").change(select_change);
}

$(document).ready(init);
