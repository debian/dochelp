(****************************************************************************)
(*  Copyright © 2012-2015 Mehdi Dogguy <mehdi@debian.org>                   *)
(*                                                                          *)
(*  This file is part of Dochelp.                                           *)
(*                                                                          *)
(*  Dochelp is free software: you can redistribute it and/or modify it      *)
(*  under the terms of the GNU General Public License as published by the   *)
(*  Free Software Foundation, either version 3 of the License, or (at your  *)
(*  option) any later version.                                              *)
(*                                                                          *)
(*  Dochelp is distributed in the hope that it will be useful, but WITHOUT  *)
(*  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *)
(*  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *)
(*  for more details.                                                       *)
(*                                                                          *)
(*  You should have received a copy of the GNU General Public License       *)
(*  along with Dochelp.  If not, see <http://www.gnu.org/licenses/>.        *)
(****************************************************************************)

open Printf
open Ocamlbuild_plugin

exception Subprocess_died_unexpectedly of Unix.process_status

let try_exec cmd =
  Sys.command (sprintf "%s >/dev/null 2>&1" cmd) = 0

let try_run cmd = (* reads one single line *)
  let in_c = Unix.open_process_in cmd in
  let line = input_line in_c in
  let _ = Unix.close_process_in in_c in
  line

let version = try_run "dpkg-parsechangelog -S Version"

let has_ocamlopt = try_exec "which ocamlopt"
let best =
  try Sys.getenv "OCAMLBEST"
  with Not_found -> if has_ocamlopt then "native" else "byte"

let () =
  dispatch begin function

    | Before_options ->
        Options.use_ocamlfind := true;
        Options.use_menhir := true;
        Options.ocaml_yaccflags := ["--explain"]

    | After_rules ->
        (* C stubs *)
        flag ["link"; "program"; "ocaml"; "byte"; "use_libdochelp"]
          (S[A"-dllib"; A"-ldochelp"]);
        dep  ["link"; "ocaml"; "use_libdochelp"] ["src/libdochelp.a"];

        (* rule for the main executable that will link all frontends  *)
        rule "src/dochelp.ml" ~deps:["src/dochelp.mlp"] ~prod:"src/dochelp.ml" begin
          fun _ _ ->
            Cmd
              (S [A"sed";
                  A"-e"; A (sprintf "s/@VERSION@/%s/" version);
                  P"src/dochelp.mlp"; Sh">"; P"src/dochelp.ml"])
        end;

    | _ -> ()
  end
