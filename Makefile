############################################################################
#  Copyright © 2012-2015 Mehdi Dogguy <mehdi@debian.org>                   #
#                                                                          #
#  This file is part of Dochelp.                                           #
#                                                                          #
#  Dochelp is free software: you can redistribute it and/or modify it      #
#  under the terms of the GNU General Public License as published by the   #
#  Free Software Foundation, either version 3 of the License, or (at your  #
#  option) any later version.                                              #
#                                                                          #
#  Dochelp is distributed in the hope that it will be useful, but WITHOUT  #
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   #
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   #
#  for more details.                                                       #
#                                                                          #
#  You should have received a copy of the GNU General Public License       #
#  along with Dochelp.  If not, see <http://www.gnu.org/licenses/>.        #
############################################################################

# Configuration
NAME := dochelp
PREFIX := /usr/local
DESTDIR:=

# Auto-detection
ifeq ($(OCAMLBEST),)
HAS_OPT := $(shell if which ocamlopt > /dev/null; then echo yes; fi)
else ifeq ($(OCAMLBEST),native)
HAS_OPT := yes
else
HAS_OPT :=
endif
OCAML_STDLIB_DIR ?= $(DESTDIR)/$(shell /usr/bin/ocamlc -where)

CLASSIC := $(if $(INSIDE_EMACS), -classic-display)
ARCH := $(if $(HAS_OPT),native,byte)
OCAMLBUILD := ocamlbuild$(CLASSIC)$(if $(HAS_OPT),, -byte-plugin)
OCAMLBUILD_ENV :=

# Build
TARGETS := src/$(NAME).$(ARCH)

# C stubs magic for bytecode
export CAML_LD_LIBRARY_PATH=$(CURDIR)/_build/src

# Installation
BINDIR := $(PREFIX)/bin

all: build man

build:
	$(OCAMLBUILD_ENV) $(OCAMLBUILD) $(TARGETS)

man: build
	./$(NAME).$(ARCH) --help=groff > $(NAME).1

typerex: OCAMLBUILD_ENV := OCAMLFIND_COMMANDS='ocamlc=ocp-ocamlc ocamlopt=ocp-ocamlopt'
typerex: build

clean:
	$(OCAMLBUILD) -clean
	-rm $(NAME).1

headers:
	headache -r -c .headache/config -h .headache/header Makefile \
		$(wildcard *.ml src/*.ml src/*.mli src/*.mly src/*.mll src/*.c media/*.js media/styles.css)

install:
	install -d $(BINDIR)
	install _build/src/$(NAME).$(ARCH) $(BINDIR)/$(NAME)
ifeq ($(HAS_OPT),)
	install -d $(OCAML_STDLIB_DIR)/stublibs
	install _build/src/dlldochelp.so $(OCAML_STDLIB_DIR)/stublibs/dlldochelp.so
endif
	install -d $(PREFIX)/share/applications/
	install dochelp.desktop $(PREFIX)/share/applications/
