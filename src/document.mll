(****************************************************************************)
(*  Copyright © 2012-2015 Mehdi Dogguy <mehdi@debian.org>                   *)
(*                                                                          *)
(*  This file is part of Dochelp.                                           *)
(*                                                                          *)
(*  Dochelp is free software: you can redistribute it and/or modify it      *)
(*  under the terms of the GNU General Public License as published by the   *)
(*  Free Software Foundation, either version 3 of the License, or (at your  *)
(*  option) any later version.                                              *)
(*                                                                          *)
(*  Dochelp is distributed in the hope that it will be useful, but WITHOUT  *)
(*  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or   *)
(*  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License   *)
(*  for more details.                                                       *)
(*                                                                          *)
(*  You should have received a copy of the GNU General Public License       *)
(*  along with Dochelp.  If not, see <http://www.gnu.org/licenses/>.        *)
(****************************************************************************)

{

  let () = Printexc.record_backtrace true

  type section =
  | Section of string
  | Format of string

  type error =
  | Required_field_missing of (section * string)
  | No_format_section
  | Unknown_field of (string * string)
  | Invalid_document

  exception Error of error

  let raise_e e = raise (Error e)

  let error_message = function
  | Required_field_missing (Section section, field) ->
      Printf.sprintf "The field \"%s\" is required in the %s section"
        field
        section
  | Required_field_missing (Format format, field) ->
      Printf.sprintf "The field \"%s\" is required for the %s format"
        field
        format
  | No_format_section ->
      Printf.sprintf "There should be at least one registered document"
  | Unknown_field (field, section) ->
      Printf.sprintf "Field \"%s\" is not allowed in section %s"
        field
        section
  | Invalid_document ->
      Printf.sprintf "Invalid document"

  let () =
    Printexc.register_printer (function
    | Error e -> Some (error_message e)
    | _ -> None)

  (* Field names are case-insensitive *)
  let fields_document = [
    "document";
    "title";
    "author";
    "abstract";
    "section";
  ]
  let fields_format = [
    "format";
    "files";
    "index";
  ]
  let fields = fields_document @ fields_format

  (* known format types *)
  let formats = [
    "html";
    "pdf";
    "text";
    "postscript";
    "info";
    "dvi";
    "debiandoc-sgml";
  ]

  type index = string
  type files = string list

  type doc_format =
    | Text
    | Pdf
    | Ps
    | Dvi
    | Debiandoc_Sgml
    | Other_type of string

  let doc_format_of_string = function
  | "text" -> Text
  | "pdf" -> Pdf
  | "ps" | "postscript" -> Ps
  | "dvi" -> Dvi
  | "debiandoc-sgml" -> Debiandoc_Sgml
  | _ as s -> Other_type s

  (* valid document formats *)
  type format =
    | Html of files * index
    | Info of files * index
    | Other of doc_format * files * index option

  let string_of_format = function
  | Html _ -> "HTML"
  | Info _ -> "Info"
  | Other (Text, _, _) -> "Text"
  | Other (Pdf, _, _) -> "PDF"
  | Other (Ps, _, _) -> "PS"
  | Other (Dvi, _, _) -> "DVI"
  | Other (Debiandoc_Sgml, _, _) -> "DebianDoc-SGML"
  | Other ((Other_type s), _, _) -> s

  (* document type *)
  type t = {
    (* Required fields *)
    id : string;
    title : string;
    section: string;

    (* Optional fields *)
    author : string option;
    abstract : string option;

    (* Registered files (len>0) *)
    documents : format list
  }

  module M = Map.Make(String)

  let required section e l =
    try
      List.assoc e l
    with Not_found ->
      List.iter (fun (k,v) -> Printf.printf "%s: %s\n%!" k v) l;
      raise_e (Required_field_missing (section,e))

  let optional e l =
    try
      Some (List.assoc e l)
    with Not_found -> None

  let opt_strip = function
  | None -> None
  | Some s ->
      let len = String.length s in
      if s.[len - 1] = '\n'
      then Some (String.sub s 0 (String.length s -1))
      else Some s

  let return_document sec_document sec_formats =
    let id, title, section =
      let required = required (Section "Document") in
      required "document" sec_document,
      required "title" sec_document,
      required "section" sec_document
    in
    let author, abstract =
      optional "author" sec_document,
      opt_strip (optional "abstract" sec_document)
    in
    let sec_formats = List.filter (fun l -> l <> []) sec_formats in
    let documents = List.map (fun par_format ->
      let required = required (Section "Format") in
      let format, index =
        required "format" par_format,
        optional "index" par_format
      in
      let files =
        List.fold_left begin fun accu (key, files) ->
          if key = "files"
          then files :: accu
          else accu
        end
        []
        par_format
      in
      let files = List.rev files
      in match format, index with
        | ("html"|"info"), None ->
            raise_e (Required_field_missing (Format format, "index"))
        | "html", Some index -> Html (files, index)
        | "info", Some index -> Info (files, index)
        | _ -> Other (doc_format_of_string format, files, index)
    ) sec_formats
    in
    if List.length documents > 0 then
      { id; title; section; author; abstract; documents; }
    else
      raise_e No_format_section

}

let space = [' ' '\t']
let field_name = ['a'-'z' 'A'-'Z' '-' '_' '0'-'9']+
let field_value = [^ '\n']*

rule read_document accu = parse
  | (field_name as field) space* ':' space* (field_value as value) '\n' {
    let field = String.lowercase_ascii field in
    let value =
      match field with
      | "abstract" -> " " ^ value ^ "\n"
      | "format" -> String.lowercase_ascii value
      | _ -> value
    in
    if List.mem field fields_document then
      read_document ((field, value)::accu) lexbuf
    else
      if List.mem field fields_format then
        read_formats accu [] [field, value] lexbuf
      else
      raise_e (Unknown_field (field, "Document"))
  }
  | (' ' | '\t') (field_value '\n' as rest) {
    match accu with
    | [] -> assert false
    | (field, value)::accu ->
        let rest = if rest = ".\n" then "\n" else rest in
        let value = Printf.sprintf "%s %s" value rest in
        read_document ((field, value)::accu) lexbuf
  }
  | (space* '\n')+ {
    read_formats accu [] [] lexbuf
  }
  | eof {
    raise_e Invalid_document
  }

and read_formats document formats format = parse
  | (field_name as field) space* ':' space* (field_value as value) '\n'? {
    let field = String.lowercase_ascii field in
    let value =
      if field = "format"
      then String.lowercase_ascii value
      else value
    in
    if List.mem field fields_format then
      read_formats document formats ((field, value)::format) lexbuf
    else
      raise_e (Unknown_field (field, "Format"))
  }
  | (' ' | '\t') (field_value '\n'? as rest) {
    read_formats document formats (("files", rest)::format) lexbuf
  }
  | (space* '\n')+ {
    read_formats document (format::formats) [] lexbuf
  }
  | eof {
    return_document document (format::formats)
  }

{
  let read file lexbuf =
    try
      read_document [] lexbuf
    with
    | Error e ->
      Printf.eprintf "E: %s: %s\n%!" file (error_message e);
      raise Exit
    | Failure s ->
      Printf.eprintf "E: Failed to read %s: %s\n%!" file s;
      raise Exit

  let print document =
    let open Utils in
        print_s "Document: %s
Title: %s
Section: %s\n"
          document.id
          document.title
          document.section;
        print_o "Author: %s\n" document.author;
        print_o "Abstract:\n%s\n" document.abstract;
        List.iter begin fun doc ->
          print_newline ();
            let format = string_of_format doc in
            match doc with
              | Html (files, index)
              | Info (files, index) ->
                  print_s "Format: %s\nIndex: %s\n"
                    format
                    index;
                  List.iter print_files files
              | Other (_, files, index) ->
                  print_s "Format: %s\n" format;
                  print_o "Index: %s\n" index;
                  List.iter print_files files
        end document.documents

  let html d =
    let open Utils in
    let open Printf in
    let open Tyxml.Html in
        let a_link uri content =
          let uri = sprintf "file://%s" uri in
          a ~a:[a_href (uri_of_string uri)] content in
        let header_p ?(l = []) t c = match t with
          | None -> div ~a:[a_class ["header"]] [ span ~a:[a_class l] [ txt c ] ]
          | Some t ->
              div ~a:[a_class ["header"]] [
                b [ txt t ];
                txt ": ";
                span ~a:[a_class l] [ txt c ];
              ]
        in
        let header_s ?(l = []) t c = header_p ~l (Some t) c in
        let header_o ?(l = []) t = function
        | None -> txt ""
        | Some c -> header_s ~l t c
        in
        let header_c = function
        | None -> txt ""
        | Some c ->
            div ~a:[a_class ["header"]] [
              pre [ txt c ];
            ]
        in
        let spandoc contents = span ~a:[a_class ["doc"]] contents in
        let documents =
          List.map begin fun doc ->
              let format = string_of_format doc in
              match doc with
              | Html (_, index)
              | Info (_, index)
              | Other (_, _, Some index) ->
                  spandoc [ a_link index [ txt format ] ]
              | Other (_, files, None) ->
                  let _, spans = List.fold_left
                    begin fun (idx, files) file ->
                      let i = string_of_int idx in
                      let elt =
                        spandoc [ a_link file [
                          txt format;
                          sub [ txt i ] ]
                        ]
                      in
                      (idx+1, elt :: files)
                    end
                    (1, [])
                    (List.flatten $ List.map glob_l files) in
                  span ~a:[a_class ["doc_container"]] spans
          end d.documents in
        div ~a:[a_id d.id; a_class ["document"]] (
            header_p ~l:["title"] None d.title ::
            header_s ~l:["section"] "Section" d.section ::
            header_o ~l:["authors"] "Authors" d.author ::
            header_c d.abstract ::
            [ div ~a:[a_class ["documents"]] documents ]
        )

  let read_file file =
    Utils.with_in_file file begin fun chan ->
      read file (Lexing.from_channel chan)
    end

  let merge doc1 doc2 =
    { doc1 with documents = doc1.documents @ doc2.documents }

  let all doc_base_dir =
    Utils.fold_files
      doc_base_dir
      begin fun accu file filename ->
        try
          let t = read_file file in begin
            try
              let doc = M.find t.id accu in
              M.add t.id (merge doc t) (M.remove t.id accu)
            with Not_found ->
              M.add t.id t accu
          end
        with Exit -> accu
      end
      M.empty

}
